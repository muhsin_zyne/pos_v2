<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_bill'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("customer_credit/add/".$customer_information->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php echo lang('bill_date', 'bill_date'); ?>
                        <div class="controls">
                            <?php echo form_input('bill_date', '', 'class="form-control date" id="bill_date" required="required"'); ?>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php echo lang('customer', 'company_id'); ?>
                        <div class="controls">
                            <?php echo form_input('company_id', $customer_information->name, 'class="form-control" id="company_id" required="required" disabled'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php echo lang('amount', 'amount'); ?>
                        <div class="controls">
                            <?php echo form_input('amount', '', 'class="form-control tip" id="amount" required="required"'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php echo lang('paid_amount', 'paid_amount'); ?>
                        <div class="controls">
                            <?php echo form_input('paid_amount', '', 'class="form-control tip" id="paid_amount" required="required"'); ?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <?php echo lang('comment', 'comment'); ?>
                <div class="controls">
                    <?php echo form_textarea($comment); ?>
                </div>
            </div>
            <div class="row">

            </div>

            
        </div>
        <div class="modal-footer">
            <?php echo form_submit('create_bill', lang('create_bill'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>

