<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 



?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_bill'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("customer_credit/add_bill/".$account_summary->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php echo lang('bill_date', 'bill_date'); ?>
                        <div class="controls">
                            <?php echo form_input('bill_date', set_value('date', $this->sma->hrld(date('Y-m-d'))), 'class="form-control datetime" id="date" required="required"'); ?>
                        </div>
                    </div>
                    
                </div>
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php echo lang('customer', 'customer'); ?>
                        <div class="controls">
                            <select class="form-control">
                                <option> <?= $account_summary->name ?> </option>
                            </select>
                        </div>
                    </div>
                    
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("stock_amount", "stock_amount") ?>
                        <?= form_input('stock_amount',  '', 'class="form-control tip" id="stock_amount" required="required"') ?>
                    </div>
                </div>
                    
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("total_sales", "total_sales") ?>
                        <?= form_input('total_sale',  '', 'class="form-control tip" id="total_sales" required="required"') ?>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("cash_paid", "cash_paid") ?>
                        <?= form_input('cash_paid',  '', 'class="form-control tip" id="cash_paid" required="required"') ?>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("credit_amount", "credit_amount") ?>
                        <?= form_input('credit_amount',  '', 'class="form-control tip" id="credit_amount" required="required"') ?>
                    </div>
                </div>
                

            </div>
            <div class="row">
                <div class="col-sm-6">
                    

                </div>
                <div class="col-sm-6">
                    

                </div>
            </div>

            
        </div>
        <div class="modal-footer">
            <?php echo form_submit('', lang('update_bill'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;



$("#total_sales").change(function() {
    var total_sales = $("#total_sales").val();
    var stock_amount = $("#stock_amount").val();
    alert(total_sales);
    if(total_sales > stock_amount) {
        total_sales = stock_amount;
        $("#total_sales").val(total_sales);
    }
});

$("#cash_paid").keyup(function() {
    formatCalcluations();
});


function formatCalcluations() {
    var stock_amount = $("#stock_amount").val();
    var total_sales = $("#total_sales").val();
    var cash_paid = $("#cash_paid").val();
    
    var credit_amount = total_sales - cash_paid;
    if(credit_amount < 0) {
        credit_amount = 0;
    }
    $("#credit_amount").val(credit_amount);
}


Number.prototype.format = function(n, x) {
          var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
          return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
      };

</script>

    