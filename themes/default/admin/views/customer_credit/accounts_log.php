<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('statements') . " (" . $customer_information->name . ")"; ?></h4>
        </div>

        <div class="modal-body">
<!--             <p class="no-print"><?= lang('deposits_subheading'); ?></p>
 -->            <div class="alerts-con"></div>

            <div class="table-responsive">
                <table id="DepData" cellpadding="0" cellspacing="0" border="0"
                       class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                    <tr class="primary">
                        <th class="col-xs-3"><?= lang("transaction_time"); ?></th>
                        <th class="col-xs-2"><?= lang("bill_date"); ?></th>
                        <th class="col-xs-1"><?= lang("refrence_id"); ?></th>
                        <th class="col-xs-2"><?= lang("credit"); ?></th>
                        <th class="col-xs-2"><?= lang("debit"); ?></th>
                        <th class="col-xs-3"><?= lang("account_manager"); ?></th>
                        <th ><?= lang("actions"); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="7" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <th class="col-xs-3"></th>
                        <th class="col-xs-2"></th>
                        <th class="col-xs-1"></th>
                        <th class="col-xs-2"></th>
                        <th class="col-xs-2"> </th>
                        <th class="col-xs-3"></th>
                        <th > </th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <?= $modal_js ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.tip').tooltip();
            oTable = $('#DepData').dataTable({
                "aaSorting": [[2, "desc"]],
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                'bProcessing': true, 'bServerSide': true,
                'sAjaxSource': '<?= admin_url('customer_credit/get_statements/'.$customer_information->id) ?>',
                'fnServerData': function (sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                },
                "aoColumns": [
                {
                    "mRender" : function ( url, type, full )  {
                        var objDate = new Date(url);
                        return formatDateTime(objDate, 'dateTime');
                        //console.log(objDate.getMonth());

                       // console.log(type);
                       // console.log(full);
                      //return  '<a href="'+url+'">' + url + '</a>';
                  }
                },
                {
                    "mRender" : function ( url, type, full )  {
                        var objDate = new Date(url);

                        return formatDateTime(objDate, 'date');
                    },

                },
                {
                    "mRender" : function ( url, type, full )  {
                        return url;
                    },

                },
                {
                    "mRender" : function ( moenyObj, type, full )  {
                        if(moenyObj!== '') {
                            return parseInt(moenyObj).format(2);

                        } else {
                            return '';
                        }
                    },
                    "sClass": "alignRight",
                },
                {
                    "mRender" : function ( moenyObj, type, full )  {
                        if(moenyObj!== '') {
                            return parseInt(moenyObj).format(2);

                        } else {
                            return '';
                        }
                  },
                  "sClass": "alignRight",
                },
                null,
                null
                //{"mRender": fld}, 
                //{"mRender": currencyFormat}, 
                //null, 
                // null, 
                // {"bSortable": false}
                ]
            });
            //$('div.dataTables_length select').addClass('form-control');
            $('div.dataTables_length select').addClass('select2');
            //$('div.dataTables_filter input').attr('placeholder', 'Date (yyyy-mm-dd)');
            $('select.select2').select2({minimumResultsForSearch: 7});
        });

        function formatDateTime(inputDate, type) {
            if(type == 'dateTime') {
                var dd = String(inputDate.getDate()).padStart(2, '0');
                var mm = String(inputDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = inputDate.getFullYear();
                var hours = inputDate.getHours();
                var minutes = inputDate.getMinutes();
                var ampm = hours >= 12 ? 'PM' : 'AM';
                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                minutes = minutes < 10 ? '0'+minutes : minutes;
                var strTime = hours + ':' + minutes + ' ' + ampm;
                return mm + '/' + dd + '/' + yyyy +' '+ strTime;
            } else {
                var dd = String(inputDate.getDate()).padStart(2, '0');
                var mm = String(inputDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = inputDate.getFullYear();
                return mm + '/' + dd + '/' + yyyy ;
            }
           }
        Number.prototype.format = function(n, x) {
          var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
          return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
      };

    </script>

    
<style type="text/css" media="screen">
    .alignRight {
        text-align: right;
    }
    div#DepData_filter {
        display: none;
    }
    
</style>



