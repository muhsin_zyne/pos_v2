    <?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <?php 
    $account_summary = $this->data['account_summary'];
    //echo '<pre>'; print_r($account_summary); echo '</pre>'; die();
    ?>
  
      <script>
        $(document).ready(function () {
            oTable = $('#NTTable').dataTable({
                "aaSorting": [[1, "asc"], [2, "asc"]],
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                'bProcessing': true, 'bServerSide': true,
                'sAjaxSource': '<?= admin_url('customer_credit/getCustomerwise?customer_id='.$account_summary->id.'') ?>',
                "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                    $("td:first", nRow).html(iDisplayIndex +1);
                   return nRow;
                },  
                'fnServerData': function (sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                },
                "aoColumns": [

                null,

                {
                    "mRender" : function ( url, type, full )  {
                        var objDate = new Date(url);
                        return formatDateTime(objDate, 'date');
                    },
                    "bSortable": true,
                    "bSearchable": false
                }, 
                null, 
                null, 
                {"bSortable": true, "sClass": "alignRight",  "bSearchable": true}, 
                {"bSortable": true,"sClass": "alignRight", "bSearchable": true},
                {"bSortable": false, "bSearchable": false}]

                       
            });
        });
    </script>

    <div class="box">
        <div class="box-header">
            <h2 class="blue"><i class="fa-fw fa fa-info-circle"></i><?= lang('account_summary_of')." ".$account_summary->name;  ?>
            <?php  ?>
             </h2>

            <div class="box-icon">
                <ul class="btn-tasks">
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
                        </a>
                        <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                            <li>
                                <a href="<?= admin_url('customer_credit/add_bill/'.$account_summary->id) ?>" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-plus-circle"></i> <?= lang('add_bill') ?>
                                </a>
                            </li>
                            <!-- <li>
                                <a href="<?= admin_url('customer_credit/adddebit/'.$account_summary->id) ?>" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-plus-circle"></i> <?= lang('add_debit_bill') ?>
                                </a>
                            </li>
                            
                            <li>
                                <a href="<?= admin_url('customer_credit/accounts_log/'.$account_summary->id) ?>" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-plus-circle"></i> <?= lang('account_statements') ?>
                                </a>
                            </li> -->
                        </ul>
                    </li>
                    <?php if (!empty($warehouses)) { ?>
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-building-o tip" data-placement="left" title="<?= lang("warehouses") ?>"></i></a>
                            <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                <li><a href="<?= admin_url('products') ?>"><i class="fa fa-building-o"></i> <?= lang('all_warehouses') ?></a></li>
                                <li class="divider"></li>
                                <?php
                                foreach ($warehouses as $warehouse) {
                                    echo '<li><a href="' . admin_url('products/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                                }
                                ?>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </div>
   
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-lg-12">

                    <p class="introtext"><?= lang('list_results'); ?></p>
<!-- 
                    <div class="table-responsive">
                        <table id="NTTable" cellpadding="0" cellspacing="0" border="0"
                               class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th> # </th>
                                <th> <?= $this->lang->line("company")?> </th>
                                <th> <?= $this->lang->line("name")?> </th>
                                <th> <?= $this->lang->line("phone")?> </th>
                                <th> <?= $this->lang->line("current_credit")?> </th>
                                <th> <?= $this->lang->line("current_debit") ?> </th>
                                <th style="width:80px;"><?php echo $this->lang->line("actions"); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                            </tr>

                            </tbody>
                        </table>
                    </div> -->
                    <!--<p><a href="<?php echo admin_url('notifications/add'); ?>" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><?php echo $this->lang->line("add_notification"); ?></a></p>-->
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-3 pull-right">
                   <table class="table table-dark">
                      <tbody>
                        <tr>
                          <th scope="row"> <?= $this->lang->line('customer_name')?></th>
                          <td> <?= $account_summary->name ?></td>
                        </tr>
                        <tr>
                          <th scope="row"> <?=$this->lang->line('company') ?> </th>
                          <td> <?= $account_summary->company ?> </td>
                        </tr>
                        <tr>
                          <th scope="row"> <?=$this->lang->line('vat_no') ?> </th>
                          <td> <?= $account_summary->vat_no ?> </td>
                        </tr>
                        <tr>
                          <th scope="row"> <?=$this->lang->line('address') ?> </th>
                          <td> <?= $account_summary->address ?> </td>
                        </tr>
                        <tr>
                          <th scope="row"> <?=$this->lang->line('city') ?> </th>
                          <td> <?= $account_summary->city ?> </td>
                        </tr>
                        <tr>
                          <th scope="row"> <?=$this->lang->line('state') ?> </th>
                          <td> <?= $account_summary->state ?> </td>
                        </tr>
                        <tr>
                          <th scope="row"> <?=$this->lang->line('postal_code') ?> </th>
                          <td> <?= $account_summary->postal_code ?> </td>
                        </tr>
                        <tr>
                          <th scope="row"> <?=$this->lang->line('country') ?> </th>
                          <td> <?= $account_summary->country ?> </td>
                        </tr> 
                        <tr>
                          <th scope="row"> <?=$this->lang->line('phone') ?> </th>
                          <td> <?= $account_summary->phone ?> </td>
                        </tr>

                        <tr>
                          <th scope="row"> <?=$this->lang->line('email') ?> </th>
                          <td> <?= $account_summary->email ?> </td>
                        </tr>
                      </tbody>
                    </table> 
                </div>
                <div class="col-xs-12 col-sm-8">



                    <div class="table-responsive">
                        <table id="NTTable" cellpadding="0" cellspacing="0" border="0"
                               class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th> # </th>
                                <th> <?= $this->lang->line("bill_date")?> </th>
                                <th> <?= $this->lang->line("customer_name")?> </th>
                                <th> <?= $this->lang->line("bill_amount")?> </th>
                                <th> <?= $this->lang->line("paid_amount")?> </th>
                                <th> <?= $this->lang->line("credit") ?> </th>
                                <th style="width:80px;"><?php echo $this->lang->line("actions"); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>



                    






                </div>
                <!-- <div class="col-xs-12 col-sm-6">
                    <div class="custom-card" style="background: #a94442">
                        <h2>  <?= number_format($account_summary->current_credit, 2) ?> </h2>
                        <p> <?= $this->lang->line('overdue_credit_found')?> </p>
                    </div>
                    <div class="custom-card" style="background: #3c763d">
                        <h2>  <?= number_format($account_summary->current_debit, 2) ?> </h2>
                        <p> <?= $this->lang->line('active_debit_so_far')?> </p>
                    </div>
                </div> -->
            </div>
            
        </div>
    </div>


<script  type="text/javascript" >
     function formatDateTime(inputDate, type) {
            if(type == 'dateTime') {
                var dd = String(inputDate.getDate()).padStart(2, '0');
                var mm = String(inputDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = inputDate.getFullYear();
                var hours = inputDate.getHours();
                var minutes = inputDate.getMinutes();
                var ampm = hours >= 12 ? 'PM' : 'AM';
                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                minutes = minutes < 10 ? '0'+minutes : minutes;
                var strTime = hours + ':' + minutes + ' ' + ampm;
                return mm + '/' + dd + '/' + yyyy +' '+ strTime;
            } else {
                var dd = String(inputDate.getDate()).padStart(2, '0');
                var mm = String(inputDate.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = inputDate.getFullYear();
                return mm + '/' + dd + '/' + yyyy ;
            }
           }
        Number.prototype.format = function(n, x) {
          var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
          return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
      };
</script>

<style type="text/css" media="screen">
    .alignRight {
        text-align: right;
    }
    .custom-card {
        padding: 10px 20px;
        margin: 10px;
        color: #fff;
    }
    .custom-card h2 {
        font-size: 26px;
    }
    
</style>
