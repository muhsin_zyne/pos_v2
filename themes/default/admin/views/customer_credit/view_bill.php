<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 



?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_bill'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("customer_credit/view_bill/".$bill_info->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php echo lang('bill_date', 'bill_date'); ?>
                        <div class="controls">
                            <?php echo form_input('bill_date', set_value('date', $this->sma->hrld($bill_info->bill_date)), 'class="form-control datetime" id="date" required="required"'); ?>
                        </div>
                    </div>
                    
                </div>
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php echo lang('customer', 'customer'); ?>
                        <div class="controls">
                            <select class="form-control">
                                <option> <?= $bill_info->company_name ?> </option>
                            </select>
                        </div>
                    </div>
                    
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("bill_amount", "bill_amount") ?>
                        <?= form_input('bill_amount',  $this->sma->formatDecimal($bill_info->bill_amount), 'class="form-control tip" id="bill_amount" required="required"') ?>
                    </div>
                </div>
                    
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("paid_amount", "paid_amount") ?>
                        <?= form_input('paid_amount',  $this->sma->formatDecimal($bill_info->paid_amount), 'class="form-control tip" id="paid_amount" required="required"') ?>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("credit_amount", "credit_amount") ?>
                        <?= form_input('credit_amount',  $this->sma->formatDecimal($bill_info->credit_amount), 'class="form-control tip" id="credit_amount" required="required"') ?>
                    </div>
                </div>
                

            </div>
            <div class="row">
                <div class="col-sm-6">
                    

                </div>
                <div class="col-sm-6">
                    

                </div>
            </div>

            
        </div>
        <div class="modal-footer">
            <?php echo form_submit('update_bill', lang('update_bill'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;


$("#bill_amount").keyup(function() {
    formatCalcluations();
});

$("#paid_amount").keyup(function() {
    formatCalcluations();
});

$("#credit_amount").keyup(function() {
    formatCalcluations();
});


function formatCalcluations() {
    var bill_amount = $("#bill_amount").val();
    var paid_amount = $("#paid_amount").val();
    var credit_amount = bill_amount - paid_amount;
    if(credit_amount < 0) {
        credit_amount = 0;
    }
    $("#credit_amount").val(credit_amount);
}


Number.prototype.format = function(n, x) {
          var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
          return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
      };

</script>

    