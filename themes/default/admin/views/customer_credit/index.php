    <?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <script>
        $(document).ready(function () {
            oTable = $('#NTTable').dataTable({
                "aaSorting": [[1, "asc"]],
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                'bProcessing': true, 'bServerSide': true,
                'sAjaxSource': '<?= admin_url('customer_credit/getAccounts') ?>',
                "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                    $("td:first", nRow).html(iDisplayIndex +1);
                   return nRow;
                },
                'fnServerData': function (sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                },

                "aoColumns": [
                    {"bSortable": false,  "bSearchable": false}, 
                    {"bSortable": false,  "bSearchable": false}, 
                    {"bSortable": false,  "bSearchable": false}, 
                    {"bSortable": false,  "bSearchable": false}, 
                    {"bSortable": false, "sClass": "alignRight",  "bSearchable": false}, 
                    {"bSortable": false,"sClass": "alignRight", "bSearchable": false},
                    {"bSortable": false, "sClass": "alignRight", "bSearchable": false},
                    {"bSortable": false,  "sClass": "alignRight", "bSearchable": false}, 
                     {"bSortable": false,  "sClass": "alignRight", "bSearchable": false}, 
                     {"bSortable": false,  "bSearchable": false}
                ],
                'fnFooterCallback': function (nRow, aaData, iStart, iEnd, aiDisplay) {
                    var gtotal = 0, paid = 0, balance = 0, stockinV = 0, stock = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        stock+=  parseFloat(aaData[aiDisplay[i]][4]);
                        stockinV+=  parseFloat(aaData[aiDisplay[i]][5]);
                        gtotal += parseFloat(aaData[aiDisplay[i]][6]);
                        paid += parseFloat(aaData[aiDisplay[i]][7]);
                        balance += parseFloat(aaData[aiDisplay[i]][8]);
                    }
                    var nCells = nRow.getElementsByTagName('th');
                    nCells[4].innerHTML = currencyFormat(parseFloat(stock));
                    nCells[5].innerHTML = currencyFormat(parseFloat(stockinV));
                    nCells[6].innerHTML = currencyFormat(parseFloat(gtotal));
                    nCells[7].innerHTML = currencyFormat(parseFloat(paid));
                    nCells[8].innerHTML = currencyFormat(parseFloat(balance));
                }

                       
            });
        });
    </script>

    <div class="box">
        <div class="box-header">
            <h2 class="blue"><i class="fa-fw fa fa-info-circle"></i><?= lang('customer_accounts'); ?></h2>

            <div class="box-icon">
                <ul class="btn-tasks">
                    <li class="dropdown"><a href="<?= admin_url('customers/add'); ?>" data-toggle="modal"
                                            data-target="#myModal"><i class="icon fa fa-plus"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-lg-12">

                    <p class="introtext"><?= lang('list_results'); ?></p>

                    <div class="table-responsive">
                        <table id="NTTable" cellpadding="0" cellspacing="0" border="0"
                               class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th> # </th>
                                <th> <?= $this->lang->line("number")?> </th>
                                <th> <?= $this->lang->line("company_name")?> </th>
                                <th> <?= $this->lang->line("phone")?> </th>
                                <th> <?= $this->lang->line("stock_amount")?> </th>
                                <th> <?= $this->lang->line("stock_in_vehicle")?> </th>
                                <th> <?= $this->lang->line("total_sales")?> </th>
                                <th> <?= $this->lang->line("total_paid") ?> </th>
                                <th> <?= $this->lang->line("total_credit") ?> </th>
                                <th style="width:80px;"><?php echo $this->lang->line("actions"); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="10" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                            </tr>

                            </tbody>
                            <tfoot class="dtFilter">
                            <tr class="active">
                                <th style="min-width:30px; width: 30px; text-align: center;">
                                </th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th> <?= lang("total_stock"); ?></th>
                                <th> <?= lang("total_stock_in_vehicle"); ?> </th>
                                <th><?= lang("total_sales"); ?></th>
                                <th><?= lang("paid"); ?></th>
                                <th><?= lang("credit"); ?></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!--<p><a href="<?php echo admin_url('notifications/add'); ?>" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><?php echo $this->lang->line("add_notification"); ?></a></p>-->
                </div>
            </div>
        </div>
    </div>



<style type="text/css" media="screen">
    .alignRight {
        text-align: right;
    }
    
</style>
