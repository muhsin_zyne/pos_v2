    ALTER TABLE
        `sma_costing` CHANGE `quantity` `quantity` DECIMAL(15, 4) NOT NULL,
        CHANGE `purchase_net_unit_cost` `purchase_net_unit_cost` DECIMAL(25, 4) NULL DEFAULT NULL,
        CHANGE `purchase_unit_cost` `purchase_unit_cost` DECIMAL(25, 4) NULL DEFAULT NULL,
        CHANGE `sale_net_unit_price` `sale_net_unit_price` DECIMAL(25, 4) NOT NULL,
        CHANGE `sale_unit_price` `sale_unit_price` DECIMAL(25, 4) NOT NULL,
        CHANGE `quantity_balance` `quantity_balance` DECIMAL(15, 4) NULL DEFAULT NULL; /* ALTER TABLE `sma_costing` ADD `option_id` INT NULL ; */
    UPDATE
        `sma_migrations`
    SET
        `version` = 307;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.1.13'
    WHERE
        `setting_id` = 1;
    /*ALTER TABLE
        `sma_settings` ADD `display_all_products` TINYINT(1) NULL DEFAULT '0'; */
    UPDATE
        `sma_migrations`
    SET
        `version` = 308;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.1.16'
    WHERE
        `setting_id` = 1;

        /*ADD `view_right` TINYINT(1) NOT NULL DEFAULT '0', */
        /*ADD `edit_right` TINYINT(1) NOT NULL DEFAULT '0'; */
    UPDATE
        `sma_migrations`
    SET
        `version` = 309;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.2.0'
    WHERE
        `setting_id` = 1;

    CREATE TABLE IF NOT EXISTS `sma_gift_card_topups`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `date` TIMESTAMP NOT NULL,
        `card_id` INT(11) NOT NULL,
        `amount` DECIMAL(15, 4) NOT NULL,
        `created_by` INT(11) NOT NULL,
        PRIMARY KEY(`id`),
        KEY `card_id`(`card_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    CREATE TABLE IF NOT EXISTS `sma_addresses`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `company_id` INT(11) NOT NULL,
        `line1` VARCHAR(50) NOT NULL,
        `line2` VARCHAR(50) NULL,
        `city` VARCHAR(25) NOT NULL,
        `postal_code` VARCHAR(20) NULL,
        `state` VARCHAR(25) NOT NULL,
        `country` VARCHAR(50) NOT NULL,
        `phone` VARCHAR(50) NULL,
        `updated_at` TIMESTAMP NOT NULL,
        PRIMARY KEY(`id`),
        KEY `company_id`(`company_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    ALTER TABLE
        `sma_products` 
        ADD INDEX(`unit`),
        ADD INDEX(`brand`);
    CREATE TABLE IF NOT EXISTS `sma_brands`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `code` VARCHAR(20) NULL,
        `name` VARCHAR(50) NOT NULL,
        `image` VARCHAR(50) NULL,
        PRIMARY KEY(`id`),
        KEY `name`(`name`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    CREATE TABLE IF NOT EXISTS `sma_price_groups`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(50) NOT NULL,
        PRIMARY KEY(`id`),
        KEY `name`(`name`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    CREATE TABLE IF NOT EXISTS `sma_product_prices`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `product_id` INT(11) NOT NULL,
        `price_group_id` INT(11) NOT NULL,
        `price` DECIMAL(25, 4) NOT NULL,
        PRIMARY KEY(`id`),
        KEY `product_id`(`product_id`),
        KEY `price_group_id`(`price_group_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    CREATE TABLE IF NOT EXISTS `sma_units`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `code` VARCHAR(10) NOT NULL,
        `name` VARCHAR(55) NOT NULL,
        `base_unit` INT(11) DEFAULT NULL,
        `operator` VARCHAR(1) DEFAULT NULL,
        `unit_value` VARCHAR(55) DEFAULT NULL,
        `operation_value` VARCHAR(55) DEFAULT NULL,
        PRIMARY KEY(`id`),
        KEY `base_unit`(`base_unit`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;

    UPDATE
        `sma_migrations`
    SET
        `version` = 316;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.2.14'
    WHERE
        `setting_id` = 1;

    CREATE TABLE IF NOT EXISTS `sma_adjustments`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `date` TIMESTAMP NOT NULL,
        `reference_no` VARCHAR(55) NOT NULL,
        `warehouse_id` INT(11) NOT NULL,
        `note` TEXT NULL,
        `attachment` VARCHAR(55) NULL,
        `created_by` INT(11) NOT NULL,
        `updated_by` INT(11) NULL,
        `updated_at` DATETIME NULL,
        PRIMARY KEY(`id`),
        KEY `warehouse_id`(`warehouse_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    INSERT
    INTO
        `sma_price_groups`(`name`)
    VALUES('Default');
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.2.15'
    WHERE
        `setting_id` = 1;
    CREATE TABLE IF NOT EXISTS `sma_stock_counts`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `date` TIMESTAMP NOT NULL,
        `reference_no` VARCHAR(55) NOT NULL,
        `warehouse_id` INT(11) NOT NULL,
        `type` VARCHAR(10) NOT NULL,
        `initial_file` VARCHAR(50) NOT NULL,
        `final_file` VARCHAR(50) NULL,
        `brands` VARCHAR(50) NULL,
        `brand_names` VARCHAR(100) NULL,
        `categories` VARCHAR(50) NULL,
        `category_names` VARCHAR(100) NULL,
        `note` TEXT NULL,
        `products` INT(11) NULL,
        `rows` INT(11) NULL,
        `differences` INT(11) NULL,
        `matches` INT(11) NULL,
        `missing` INT(11) NULL,
        `created_by` INT(11) NOT NULL,
        `updated_by` INT(11) NULL,
        `updated_at` DATETIME NULL,
        `finalized` TINYINT(1) NULL,
        PRIMARY KEY(`id`),
        KEY `warehouse_id`(`warehouse_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    CREATE TABLE IF NOT EXISTS `sma_stock_count_items`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `stock_count_id` INT(11) NOT NULL,
        `product_id` INT(11) NOT NULL,
        `product_code` VARCHAR(50) NULL,
        `product_name` VARCHAR(255) NULL,
        `product_variant` VARCHAR(55) NULL,
        `product_variant_id` INT(11) NULL,
        `expected` DECIMAL(15, 4) NOT NULL,
        `counted` DECIMAL(15, 4) NOT NULL,
        `cost` DECIMAL(25, 4) NOT NULL,
        PRIMARY KEY(`id`),
        KEY `stock_count_id`(`stock_count_id`),
        KEY `product_id`(`product_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    ALTER TABLE
        `sma_settings` CHANGE `disable_editing` `disable_editing` SMALLINT NULL DEFAULT '90';
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.2.17'
    WHERE
        `setting_id` = 1;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.2.18'
    WHERE
        `setting_id` = 1;
    UPDATE
        `sma_migrations`
    SET
        `version` = 310;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.2.2'
    WHERE
        `setting_id` = 1;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.2.20'
    WHERE
        `setting_id` = 1;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.2.22'
    WHERE
        `setting_id` = 1;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.2.23'
    WHERE
        `setting_id` = 1;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.2.24'
    WHERE
        `setting_id` = 1;
    UPDATE
        `sma_migrations`
    SET
        `version` = 311;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.2.3'
    WHERE
        `setting_id` = 1;
    CREATE TABLE IF NOT EXISTS `sma_deposits`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `company_id` INT(11) NOT NULL,
        `amount` DECIMAL(25, 4) NOT NULL,
        `paid_by` VARCHAR(50) NULL,
        `note` VARCHAR(255) NULL,
        `created_by` INT NOT NULL,
        `updated_by` INT NULL,
        `updated_at` DATETIME NULL,
        PRIMARY KEY(`id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    UPDATE
        `sma_migrations`
    SET
        `version` = 312;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.2.4'
    WHERE
        `setting_id` = 1;
    ALTER TABLE
        `sma_pos_settings` ADD `authorize` TINYINT(1) NULL DEFAULT '0';
    ALTER TABLE
        `sma_purchases` ADD `return_id` INT(11) DEFAULT NULL,
        ADD `surcharge` DECIMAL(25, 4) NOT NULL DEFAULT '0.00';
    ALTER TABLE
        `sma_quotes` ADD `supplier_id` INT NULL,
        ADD `supplier` VARCHAR(55) NULL;
    ALTER TABLE
        `sma_order_ref` ADD `rep` INT(11) NOT NULL DEFAULT '1';
    CREATE TABLE IF NOT EXISTS `sma_return_purchase_items`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `purchase_id` INT(11) UNSIGNED NOT NULL,
        `return_id` INT(11) UNSIGNED NOT NULL,
        `purchase_item_id` INT(11) DEFAULT NULL,
        `product_id` INT(11) UNSIGNED NOT NULL,
        `product_code` VARCHAR(55) NOT NULL,
        `product_name` VARCHAR(255) NOT NULL,
        `product_type` VARCHAR(20) DEFAULT NULL,
        `option_id` INT(11) DEFAULT NULL,
        `net_unit_cost` DECIMAL(25, 4) NOT NULL,
        `quantity` DECIMAL(15, 4) DEFAULT '0.00',
        `warehouse_id` INT(11) DEFAULT NULL,
        `item_tax` DECIMAL(25, 4) DEFAULT NULL,
        `tax_rate_id` INT(11) DEFAULT NULL,
        `tax` VARCHAR(55) DEFAULT NULL,
        `discount` VARCHAR(55) DEFAULT NULL,
        `item_discount` DECIMAL(25, 4) DEFAULT NULL,
        `subtotal` DECIMAL(25, 4) NOT NULL,
        `real_unit_cost` DECIMAL(25, 4) NULL,
        PRIMARY KEY(`id`),
        KEY `purchase_id`(`purchase_id`),
        KEY `product_id`(`product_id`),
        KEY `product_id_2`(`product_id`, `purchase_id`),
        KEY `purchase_id_2`(`purchase_id`, `product_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    CREATE TABLE IF NOT EXISTS `sma_return_purchases`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `purchase_id` INT(11) NOT NULL,
        `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `reference_no` VARCHAR(55) NOT NULL,
        `supplier_id` INT(11) NOT NULL,
        `supplier` VARCHAR(55) NOT NULL,
        `warehouse_id` INT(11) DEFAULT NULL,
        `note` VARCHAR(1000) DEFAULT NULL,
        `total` DECIMAL(25, 4) NOT NULL,
        `product_discount` DECIMAL(25, 4) DEFAULT '0.00',
        `order_discount_id` VARCHAR(20) DEFAULT NULL,
        `total_discount` DECIMAL(25, 4) DEFAULT '0.00',
        `order_discount` DECIMAL(25, 4) DEFAULT '0.00',
        `product_tax` DECIMAL(25, 4) DEFAULT '0.00',
        `order_tax_id` INT(11) DEFAULT NULL,
        `order_tax` DECIMAL(25, 4) DEFAULT '0.00',
        `total_tax` DECIMAL(25, 4) DEFAULT '0.00',
        `surcharge` DECIMAL(25, 4) DEFAULT '0.00',
        `grand_total` DECIMAL(25, 4) NOT NULL,
        `created_by` INT(11) DEFAULT NULL,
        `updated_by` INT(11) DEFAULT NULL,
        `updated_at` TIMESTAMP NULL DEFAULT NULL,
        `attachment` VARCHAR(55) NULL,
        PRIMARY KEY(`id`),
        KEY `id`(`id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    CREATE TABLE IF NOT EXISTS `sma_expense_categories`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `code` VARCHAR(55) NOT NULL,
        `name` VARCHAR(55) NOT NULL,
        PRIMARY KEY(`id`),
        KEY `id`(`id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    ALTER TABLE
        `sma_expenses` ADD `category_id` INT NULL;
    ALTER TABLE
        `sma_permissions` ADD `products-barcode` TINYINT(1) NOT NULL DEFAULT '0',
        ADD `purchases-return_purchases` TINYINT(1) NOT NULL DEFAULT '0',
        ADD `reports-expenses` TINYINT(1) NOT NULL DEFAULT '0';
    UPDATE
        `sma_migrations`
    SET
        `version` = 313;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.2.6'
    WHERE
        `setting_id` = 1;
    ALTER TABLE
        `sma_settings` ADD `set_focus` TINYINT(1) NOT NULL DEFAULT '0';
    ALTER TABLE
        `sma_warehouses_products` ADD `avg_cost` DECIMAL(25, 4) NOT NULL;
    ALTER TABLE
        `sma_permissions` ADD `reports-daily_purchases` TINYINT(1) NULL DEFAULT '0',
        ADD `reports-monthly_purchases` TINYINT(1) NULL DEFAULT '0';
    ALTER TABLE
        `sma_expenses` ADD `warehouse_id` INT NULL;
    ALTER TABLE
        `sma_sales` ADD `return_sale_ref` VARCHAR(55) NULL,
        ADD `sale_id` INT NULL,
        ADD `return_sale_total` DECIMAL(25, 4) NOT NULL DEFAULT '0';
    ALTER TABLE
        `sma_sale_items` ADD `sale_item_id` INT NULL;
    ALTER TABLE
        `sma_purchases` ADD `return_purchase_ref` VARCHAR(55) NULL,
        ADD `purchase_id` INT NULL,
        ADD `return_purchase_total` DECIMAL(25, 4) NOT NULL DEFAULT '0';
    ALTER TABLE
        `sma_purchase_items` ADD `purchase_item_id` INT NULL;
    ALTER TABLE
        sma_calendar
    DROP PRIMARY KEY
        ;
    ALTER TABLE
        `sma_calendar` CHANGE `date` `start` DATETIME NOT NULL,
        CHANGE `data` `title` VARCHAR(55) NOT NULL,
        ADD `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        ADD `end` DATETIME NULL,
        ADD `description` VARCHAR(255) NULL,
        ADD `color` VARCHAR(7) NOT NULL;
    UPDATE
        `sma_migrations`
    SET
        `version` = 315;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.0.2.9'
    WHERE
        `setting_id` = 1;
    ALTER TABLE
        `sma_categories` ADD `slug` VARCHAR(55) NOT NULL;
    UPDATE
        `sma_categories`
    SET
        slug =
    REPLACE
        (LOWER(NAME),
        ' ',
        '-');
    ALTER TABLE
        `sma_brands` ADD `slug` VARCHAR(55) NOT NULL;
    UPDATE
        `sma_brands`
    SET
        slug =
    REPLACE
        (LOWER(NAME),
        ' ',
        '-');
    ALTER TABLE
        `sma_products` ADD `slug` VARCHAR(55) NOT NULL,
        ADD `featured` TINYINT(1) NULL,
        ADD `weight` DECIMAL(10, 4) NULL;
    UPDATE
        `sma_products`
    SET
        slug =
    REPLACE
        (LOWER(NAME),
        ' ',
        '-');
    ALTER TABLE
        `sma_currencies` ADD `symbol` VARCHAR(50) NULL;
    ALTER TABLE
        `sma_settings` ADD `apis` TINYINT(1) NOT NULL DEFAULT '0';
    ALTER TABLE
        `sma_companies` CHANGE `address` `address` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
        CHANGE `city` `city` VARCHAR(55) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
        CHANGE `phone` `phone` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
    ALTER TABLE
        `sma_sales` ADD `api` TINYINT(1) NULL,
        ADD `shop` TINYINT(1) NULL,
        ADD `address_id` INT NULL,
        ADD `reserve_id` INT NULL,
        ADD `hash` VARCHAR(255) NULL;
    ALTER TABLE
        `sma_quotes` ADD `hash` VARCHAR(255) NULL;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.1.0'
    WHERE
        `setting_id` = 1;
    ALTER TABLE
        `sma_products` ADD `second_name` VARCHAR(255) NULL;
    DROP TABLE IF EXISTS
        `sma_returns`;
    CREATE TABLE IF NOT EXISTS `sma_returns`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `reference_no` VARCHAR(55) NOT NULL,
        `customer_id` INT(11) NOT NULL,
        `customer` VARCHAR(55) NOT NULL,
        `biller_id` INT(11) NOT NULL,
        `biller` VARCHAR(55) NOT NULL,
        `warehouse_id` INT(11) DEFAULT NULL,
        `note` VARCHAR(1000) DEFAULT NULL,
        `staff_note` VARCHAR(1000) DEFAULT NULL,
        `total` DECIMAL(25, 4) NOT NULL,
        `product_discount` DECIMAL(25, 4) DEFAULT '0.00',
        `order_discount_id` VARCHAR(20) DEFAULT NULL,
        `total_discount` DECIMAL(25, 4) DEFAULT '0.00',
        `order_discount` DECIMAL(25, 4) DEFAULT '0.00',
        `product_tax` DECIMAL(25, 4) DEFAULT '0.00',
        `order_tax_id` INT(11) DEFAULT NULL,
        `order_tax` DECIMAL(25, 4) DEFAULT '0.00',
        `total_tax` DECIMAL(25, 4) DEFAULT '0.00',
        `grand_total` DECIMAL(25, 4) NOT NULL,
        `created_by` INT(11) DEFAULT NULL,
        `updated_by` INT(11) DEFAULT NULL,
        `updated_at` TIMESTAMP NULL DEFAULT NULL,
        `total_items` SMALLINT DEFAULT NULL,
        `paid` DECIMAL(25, 4) DEFAULT '0.00',
        `surcharge` DECIMAL(25, 4) NOT NULL DEFAULT '0.00',
        `attachment` VARCHAR(55) NULL,
        `hash` VARCHAR(255) NULL,
        `cgst` DECIMAL(25, 4) NULL,
        `sgst` DECIMAL(25, 4) NULL,
        `igst` DECIMAL(25, 4) NULL,
        PRIMARY KEY(`id`),
        KEY `id`(`id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    DROP TABLE IF EXISTS
        `sma_return_items`;
    CREATE TABLE IF NOT EXISTS `sma_return_items`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `return_id` INT(11) UNSIGNED NOT NULL,
        `product_id` INT(11) UNSIGNED NOT NULL,
        `product_code` VARCHAR(55) NOT NULL,
        `product_name` VARCHAR(255) NOT NULL,
        `product_type` VARCHAR(20) DEFAULT NULL,
        `option_id` INT(11) DEFAULT NULL,
        `net_unit_price` DECIMAL(25, 4) NOT NULL,
        `unit_price` DECIMAL(25, 4) DEFAULT NULL,
        `quantity` DECIMAL(15, 4) NOT NULL,
        `warehouse_id` INT(11) DEFAULT NULL,
        `item_tax` DECIMAL(25, 4) DEFAULT NULL,
        `tax_rate_id` INT(11) DEFAULT NULL,
        `tax` VARCHAR(55) DEFAULT NULL,
        `discount` VARCHAR(55) DEFAULT NULL,
        `item_discount` DECIMAL(25, 4) DEFAULT NULL,
        `subtotal` DECIMAL(25, 4) NOT NULL,
        `serial_no` VARCHAR(255) DEFAULT NULL,
        `real_unit_price` DECIMAL(25, 4) NULL,
        `product_unit_id` INT(11) NULL,
        `product_unit_code` VARCHAR(10) NULL,
        `unit_quantity` DECIMAL(15, 4) NOT NULL,
        `comment` VARCHAR(255) NULL,
        `gst` VARCHAR(20) NULL,
        `cgst` DECIMAL(25, 4) NULL,
        `sgst` DECIMAL(25, 4) NULL,
        `igst` DECIMAL(25, 4) NULL,
        PRIMARY KEY(`id`),
        KEY `return_id`(`return_id`),
        KEY `product_id`(`product_id`),
        KEY `product_id_2`(`product_id`, `return_id`),
        KEY `return_id_2`(`return_id`, `product_id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    ALTER TABLE
        `sma_permissions` ADD `returns-index` TINYINT(1) NULL DEFAULT '0',
        ADD `returns-add` TINYINT(1) NULL DEFAULT '0',
        ADD `returns-edit` TINYINT(1) NULL DEFAULT '0',
        ADD `returns-delete` TINYINT(1) NULL DEFAULT '0',
        ADD `returns-email` TINYINT(1) NULL DEFAULT '0',
        ADD `returns-pdf` TINYINT(1) NULL DEFAULT '0',
        ADD `reports-tax` TINYINT(1) NULL DEFAULT '0';
    UPDATE
        `sma_settings`
    SET
        `version` = '3.2.12'
    WHERE
        `setting_id` = 1;
    ALTER TABLE
        `sma_sales` ADD `payment_method` VARCHAR(55) NULL;
    ALTER TABLE
        `sma_settings` CHANGE `barcode_separator` `barcode_separator` VARCHAR(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '-';
    UPDATE
        `sma_settings`
    SET
        `version` = '3.2.16'
    WHERE
        `setting_id` = 1;
    ALTER TABLE
        `sma_purchases` ADD `cgst` DECIMAL(25, 4) NULL,
        ADD `sgst` DECIMAL(25, 4) NULL,
        ADD `igst` DECIMAL(25, 4) NULL;
    ALTER TABLE
        `sma_purchase_items` ADD `gst` VARCHAR(20) NULL,
        ADD `cgst` DECIMAL(25, 4) NULL,
        ADD `sgst` DECIMAL(25, 4) NULL,
        ADD `igst` DECIMAL(25, 4) NULL;
    ALTER TABLE
        `sma_sales` ADD `cgst` DECIMAL(25, 4) NULL,
        ADD `sgst` DECIMAL(25, 4) NULL,
        ADD `igst` DECIMAL(25, 4) NULL;
    ALTER TABLE
        `sma_sale_items` ADD `gst` VARCHAR(20) NULL,
        ADD `cgst` DECIMAL(25, 4) NULL,
        ADD `sgst` DECIMAL(25, 4) NULL,
        ADD `igst` DECIMAL(25, 4) NULL;
    ALTER TABLE
        `sma_quotes` ADD `cgst` DECIMAL(25, 4) NULL,
        ADD `sgst` DECIMAL(25, 4) NULL,
        ADD `igst` DECIMAL(25, 4) NULL;
    ALTER TABLE
        `sma_quote_items` ADD `gst` VARCHAR(20) NULL,
        ADD `cgst` DECIMAL(25, 4) NULL,
        ADD `sgst` DECIMAL(25, 4) NULL,
        ADD `igst` DECIMAL(25, 4) NULL;
    ALTER TABLE
        `sma_transfers` ADD `cgst` DECIMAL(25, 4) NULL,
        ADD `sgst` DECIMAL(25, 4) NULL,
        ADD `igst` DECIMAL(25, 4) NULL;
    ALTER TABLE
        `sma_transfer_items` ADD `gst` VARCHAR(20) NULL,
        ADD `cgst` DECIMAL(25, 4) NULL,
        ADD `sgst` DECIMAL(25, 4) NULL,
        ADD `igst` DECIMAL(25, 4) NULL;
    ALTER TABLE
        `sma_products` ADD `hsn_code` INT NULL,
        ADD `views` INT(11) NULL DEFAULT '0',
        ADD `hide` TINYINT(1) NULL DEFAULT '0';
    ALTER TABLE
        `sma_settings` ADD `state` VARCHAR(100) NULL,
        ADD `pdf_lib` VARCHAR(20) NULL DEFAULT 'mpdf';
    UPDATE
        `sma_settings`
    SET
        `version` = '3.2.5'
    WHERE
        `setting_id` = 1;
    ALTER TABLE
        `sma_products` CHANGE `hide` `hide` TINYINT(1) NOT NULL DEFAULT '0';
    UPDATE
        `sma_settings`
    SET
        `version` = '3.2.8'
    WHERE
        `setting_id` = 1;
    ALTER TABLE
        `sma_brands` ADD `description` VARCHAR(255) NULL;
    ALTER TABLE
        `sma_categories` ADD `description` VARCHAR(255) NULL;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.3.6'
    WHERE
        `setting_id` = 1;
    ALTER TABLE
        `sma_companies` ADD `gst_no` VARCHAR(100) NULL;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.4.0'
    WHERE
        `setting_id` = 1;
    UPDATE
        `sma_settings`
    SET
        `version` = '3.4.11'
    WHERE
        `setting_id` = 1;
    ALTER TABLE
        `sma_suspended_items` ADD `comment` VARCHAR(255) NULL;
    ALTER TABLE
        `sma_suspended_bills` ADD `shipping` DECIMAL(15, 4) NULL DEFAULT '0';
    ALTER TABLE
        `sma_pos_settings` ADD `remote_printing` TINYINT(1) NULL DEFAULT '1',
        ADD `printer` INT(11) NULL,
        ADD `order_printers` VARCHAR(55) NULL,
        ADD `auto_print` TINYINT(1) NULL DEFAULT '0',
        ADD `customer_details` TINYINT(1) NULL;
    CREATE TABLE IF NOT EXISTS `sma_printers`(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `title` VARCHAR(55) NOT NULL,
        `type` VARCHAR(25) NOT NULL,
        `profile` VARCHAR(25) NOT NULL,
        `char_per_line` TINYINT UNSIGNED NULL,
        `path` VARCHAR(255) DEFAULT NULL,
        `ip_address` VARBINARY(45) DEFAULT NULL,
        `port` VARCHAR(10) DEFAULT NULL,
        PRIMARY KEY(`id`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
    UPDATE
        `sma_pos_settings`
    SET
        `version` = '3.0.2.23'
    WHERE
        `pos_id` = 1;
    UPDATE
        `sma_pos_settings`
    SET
        `version` = '3.0.2.24'
    WHERE
        `pos_id` = 1;
    ALTER TABLE
        `sma_pos_settings` ADD `local_printers` TINYINT(1) NULL;
    UPDATE
        `sma_pos_settings`
    SET
        `version` = '3.1.0'
    WHERE
        `pos_id` = 1;
    UPDATE
        `sma_pos_settings`
    SET
        `version` = '3.4.11'
    WHERE
        `pos_id` = 1;