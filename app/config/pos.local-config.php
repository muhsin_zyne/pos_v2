<?php 

return [
	'config' => [
		'base_url' => 'http://pos.local/'
	],
	'db' => [
		'dsn'	=> '',
		'hostname' => 'localhost',
		'username' => 'root',
		'password' => '',
		'database' => 'pos_v2_development',
	],
];

