<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Notifications
 * 
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */
$lang['current_credit']				= "Current Credit";
$lang['current_debit']				= "Current Debit";
$lang['view_customer_account']		= "View Customer Account";
$lang['accounts']               	= "Accounts";
$lang['account_summary_of']			= "Account Summary of ";
$lang['customer_credit']			= "Customer Credit";
$lang['customer_credit_details']	= "Customer Credit Details";
$lang['customer_name']				= "Customer Name";
$lang['active_due_credit']			= "Active Due Credit";
$lang['active_debit_so_far']		= "Active Debit so far";
$lang['overdue_credit_found']		= "Overdue Credit found";
$lang['add_credit_bill']			= "Add Credit Bill";
$lang['add_debit_bill']				= "Add Debit Bill";
$lang['debit_amount']				= "Add Debit Amount";
$lang['amount']						= "Amount";
$lang['add_bill']					= "Add Bill";
$lang['bill_date']					= "Bill Date";
$lang['bill_amount']				= "Bill Amount";
$lang['paid_amount']				= "Paid Amount";
$lang['create_bill']				= "Create Bill";
$lang['create_debit']				= "Create Debit";
$lang['credit_amount']				= "Credit Amount";
$lang['account_statements']			= "Account Statements";
$lang['statements']					= "Statements";
$lang['transaction_time']			= "Transaction Time";
$lang['credit']						= "Credit";
$lang['debit']						= "Debit";
$lang['account_manager']			= "Account Manager";
$lang['customer_bill_success']		= "New Credit Bill has been added to the customer";
$lang['delete_transaciton']			= "Delete Transaction";
$lang['transaction_deleted']		= "Transaction Deleted";
$lang['add_notification']           = "Add Notifications";
$lang['edit_notification']          = "Edit Notifications";
$lang['delete_notification']        = "Delete Notifications";
$lang['delete_notifications']       = "Delete Notifications";
$lang['notification_added']         = "Notifications successfully added";
$lang['notification_updated']       = "Notifications successfully updated";
$lang['notification_deleted']       = "Notifications successfully deleted";
$lang['notifications_deleted']      = "Notifications successfully deleted";
$lang['submitted_at']               = "Submitted at";
$lang['till']                       = "Till";
$lang['comment']                    = "comment";
$lang['for_customers_only']         = "For Customers only";
$lang['for_staff_only']             = "For Staff only";
$lang['for_both']                   = "For Both";
$lang['till']                       = "Till";
$lang['refrence_id']				= "Refrence ID";