<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_credit extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->admin_load('accounts', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('customer_accounts_model');
        $this->load->admin_model('customer_bill_model');

    }

    function index()
    {
        $this->sma->checkPermissions();
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('customer_accounts')));
        $meta = array('page_title' => lang('customer_accounts'), 'bc' => $bc);
        //$this->data['customer_acc_total'] = $this->customer_accounts_model->getTotalCreditReport();

        $this->page_construct('customer_credit/index', $meta, $this->data);
    }

    function getAccounts()
    {

        $this->sma->checkPermissions('index');
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
            ->select("*")
            ->from("v_customer_accounts as customer")
            //->join('customer_accounts as customer_accounts', 'customer_accounts.company_id = companies.id', 'left')
            //->where('group_name', 'customer')
           // ->group_by('companies.id')
            ->add_column("Actions", 
                "<a class=\"tip\" title='" . lang("view_customer_account") . "' href='" . admin_url('customer_credit/view/$1') . "' ><i class=\"fa fa-eye\"></i></a>", "customer.company_id");
        //->unset_column('id');
        echo $this->datatables->generate();
    }


    function getCustomerwise($customer_id = NULL)
    {
        
        $customer_id = $this->input->get('customer_id');
        $this->sma->checkPermissions('index');
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
            ->select("
                customer_bill.id,
                customer_bill.bill_date,
                companies.company,
                customer_bill.bill_amount,
                customer_bill.paid_amount,
                customer_bill.credit_amount,
            ")

            ->from("customer_bill")
            ->join('companies as companies', 'companies.id = customer_bill.company_id', 'left')
            ->where('customer_bill.company_id', $customer_id)

            //->where('group_name', 'customer')
            //->group_by('companies.id')
            ->add_column("Actions", 
                "<a class=\"tip\" title='" . lang("view_customer_account") . "' href='" . admin_url('customer_credit/view_bill/$1') . "' data-toggle=\"modal\" data-target=\"#myModal\" ><i class=\"fa fa-eye\"></i></a>", "customer_bill.id");
        //->unset_column('id');
        echo $this->datatables->generate();
    }

    function view_bill($bill_id) {
        $this->sma->checkPermissions('index');
        $bill_info = $this->customer_bill_model->findOne($bill_id);

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $data = (object) array(
                'id' => $bill_id,
                'bill_date' => date('Y-m-d', strtotime( str_replace("/", "-", $this->input->post('bill_date')) )),
                'bill_amount' => $this->input->post('bill_amount'),
                'paid_amount' => $this->input->post('paid_amount'),
                'credit_amount' => $this->input->post('credit_amount')

            );
            $bill_info->bill_date = $data->bill_date;
            $bill_info->bill_amount = $data->bill_amount;
            $bill_info->paid_amount = $data->paid_amount;
            $bill_info->credit_amount = $data->credit_amount;
            $request = $this->customer_bill_model->saveRequest($bill_info);
            admin_redirect('customer_credit/view/'.$bill_info->company_id);

        } else {

             $this->data['comment'] = array('name' => 'comment',
                    'id' => 'comment',
                    'type' => 'textarea',
                    'class' => 'form-control',
                    'value' => $this->form_validation->set_value('comment'),
                );

            $this->data['error'] = validation_errors();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['bill_info'] = $bill_info;
            $this->load->view($this->theme . 'customer_credit/view_bill', $this->data);
        }
        //echo '<pre>'; print_r($this->customer_bill_model); echo '</pre>'; die();
        

    }

    function add_bill($customer_id) {
        $this->sma->checkPermissions('index');
        $account_summary = $this->customer_accounts_model->getAccountDetails($customer_id);

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            //echo '<pre>'; print_r($this->input->post()); echo '</pre>'; die();
            $data = (object) array(
                'bill_date' => date('Y-m-d', strtotime( str_replace("/", "-", $this->input->post('bill_date')) )),
                'bill_amount' => $this->input->post('bill_amount'),
                'paid_amount' => $this->input->post('paid_amount'),
                'company_id' => $customer_id,
                'credit_amount' => $this->input->post('credit_amount'),
                'comment' =>    '',
                'created_by' =>  $this->session->userdata('user_id')
            );

            $insert = $this->customer_bill_model->insertData($data);
            admin_redirect('customer_credit/view/'.$account_summary->id);

        } else {

             $this->data['comment'] = array('name' => 'comment',
                    'id' => 'comment',
                    'type' => 'textarea',
                    'class' => 'form-control',
                    'value' => $this->form_validation->set_value('comment'),
                );

            $this->data['error'] = validation_errors();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['account_summary'] = $account_summary;
            $this->load->view($this->theme . 'customer_credit/add_bill', $this->data);
        }
        //echo '<pre>'; print_r($this->customer_bill_model); echo '</pre>'; die();
        

    }

    function view($customer_id = NULL) {
        $this->sma->checkPermissions('index');
        if ($this->input->get('customer_id')) {
            $customer_id = $this->input->get('customer_id');
        }

        $account_summary = $this->customer_accounts_model->getAccountDetails($customer_id);
        $this->data['account_summary'] = $account_summary;
        $bc = array(
            array('link' => base_url(), 'page' => lang('home')), 
            array('link' => admin_url('customer_credit'), 'page' => lang('customer_credit')),
            array('link' => '#', 'page' => $customer_id)
        );
        $meta = array('page_title' => lang('customer_credit_details'), 'bc' => $bc);
       
        $this->page_construct('customer_credit/view', $meta, $this->data);
    }

    function add($customer_id)
    {   

        $this->sma->checkPermissions();
        $customer_information = $this->companies_model->getCompanyByID($customer_id);
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $data = array(
                'comment' => $this->input->post('comment'),
                'bill_date' => $this->input->post('bill_date') ? $this->sma->fld($this->input->post('bill_date')) : NULL,
                'company_id' => $customer_id,
                'amount' => $this->input->post('amount'),
                'paid_amount' => $this->input->post('paid_amount'),
                'type' => 'credit',
                'collected_by' => $this->session->userdata('user_id'),
                'created_by' => $this->session->userdata('user_id'),
                'updated_by' => Null,
            );

            $save = $this->customer_accounts_model->formattBillData($data);
            $this->session->set_flashdata('success', lang('customer_bill_sucess'). $customer_information->name);
            admin_redirect('customer_credit/view/'.$customer_id);



        } else {
            
            $this->data['comment'] = array('name' => 'comment',
                'id' => 'comment',
                'type' => 'textarea',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('comment'),
            );

            $this->data['error'] = validation_errors();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['customer_information'] = $customer_information;
            $this->load->view($this->theme . 'customer_credit/add', $this->data);

        }
    }

    function adddebit($customer_id) {

        $this->sma->checkPermissions('add');
        $customer_information = $this->companies_model->getCompanyByID($customer_id);
       
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $data = array(
                'comment' => $this->input->post('comment'),
                'bill_date' => $this->input->post('bill_date') ? $this->sma->fld($this->input->post('bill_date')) : NULL,
                'company_id' => $customer_id,
                'amount' => $this->input->post('amount'),
                'type' => 'debit',
                'collected_by' => $this->session->userdata('user_id'),
                'created_by' => $this->session->userdata('user_id'),
                'updated_by' => Null,
            );

            $save = $this->customer_accounts_model->addDebitBill($data);
            $this->session->set_flashdata('success', lang('customer_bill_sucess'). $customer_information->name);
            admin_redirect('customer_credit/view/'.$customer_id);



        } else {
            
            $this->data['comment'] = array('name' => 'comment',
                'id' => 'comment',
                'type' => 'textarea',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('comment'),
            );

            $this->data['error'] = validation_errors();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['customer_information'] = $customer_information;
            $this->load->view($this->theme . 'customer_credit/adddebit', $this->data);

        }
    }


    function accounts_log($customer_id = NULL)
    {
        $this->sma->checkPermissions('report');
        if ($this->input->get('id')) {
            $customer_id = $this->input->get('customer_id');
        }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['customer_information'] = $this->companies_model->getCompanyByID($customer_id);
        //$this->data['statement_summary'] =  $this->customer_accounts_model->getStatementSum($customer_id);

        $this->load->view($this->theme . 'customer_credit/accounts_log', $this->data);

    }

    function get_statements($customer_id = NULL)
    {
        
        $this->sma->checkPermissions('report');
        $this->load->library('datatables');
        $this->datatables
            ->select("created_at, bill_date, customer_accounts.id as refrence_no,
            CASE WHEN amount  < 0 then abs(amount) ELSE '' END as credit,
            CASE WHEN amount  > 0 then abs(amount) ELSE '' END as debit,
            CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by,
            customer_accounts.id as id
            ")
            ->from("customer_accounts")
            ->join('users', 'users.id = customer_accounts.collected_by')
            ->where('customer_accounts.company_id', $customer_id)
            ->add_column("Actions", "<div class=\"text-center\">
                <a href='#' class='tip po' title='<b>" . lang("delete_transaciton") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('customer_credit/delete_transaciton/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id")
        ->unset_column('id');
        echo $this->datatables->generate();
    }





    function edit($id = NULL)
    {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->input->post('id')) {
            $id = $this->input->post('id');
        }

        $this->form_validation->set_rules('comment', lang("notifications"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'comment' => $this->input->post('comment'),
                'from_date' => $this->input->post('from_date') ? $this->sma->fld($this->input->post('from_date')) : NULL,
                'till_date' => $this->input->post('to_date') ? $this->sma->fld($this->input->post('to_date')) : NULL,
                'scope' => $this->input->post('scope'),
            );
        } elseif ($this->input->post('submit')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("notifications");
        }

        if ($this->form_validation->run() == true && $this->cmt_model->updateNotification($id, $data)) {

            $this->session->set_flashdata('message', lang("notification_updated"));
            admin_redirect("notifications");

        } else {

            $comment = $this->cmt_model->getCommentByID($id);

            $this->data['comment'] = array('name' => 'comment',
                'id' => 'comment',
                'type' => 'textarea',
                'class' => 'form-control',
                'required' => 'required',
                'value' => $this->form_validation->set_value('comment', $comment->comment),
            );


            $this->data['notification'] = $comment;
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['error'] = validation_errors();
            $this->load->view($this->theme . 'notifications/edit', $this->data);

        }
    }

    public function delete_transaciton($id = NULL)
    {
        $this->sma->checkPermissions('delete');
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->customer_accounts_model->deleteItem($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("transaction_deleted")));
        }
    }

}
