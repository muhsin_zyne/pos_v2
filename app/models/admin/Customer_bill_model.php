<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_bill_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function findOne($id = NULL) {
        return $this->db->query("
            SELECT 
                c_bill.id,
                c_bill.bill_date,
                c_bill.company_id,
                c.name as company_name,
                c.company as company_company,
                c_bill.bill_amount,
                c_bill.paid_amount,
                c_bill.credit_amount,
                c_bill.comment,
                c_bill.created_by

            from sma_customer_bill as c_bill
            left join sma_companies as c on c.id = c_bill.company_id

            WHERE c_bill.id = '$id'
        ")->row();
    }

    public function saveRequest($input = array()) {

        $data = $this->db->update('customer_bill', 
            array(
                'bill_date' => $input->bill_date, 
                'bill_amount' => $input->bill_amount,
                'paid_amount' => $input->paid_amount,
                'credit_amount' => $input->credit_amount
                ), array('id' => $input->id));
        return $data;
            
    }
    public function insertData($input) {
        $data = $this->db->insert('customer_bill', $input);
        return $data;
    }
 }
