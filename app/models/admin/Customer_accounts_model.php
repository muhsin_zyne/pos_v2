<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_accounts_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAccountDetails($company_id = 0) {
    	$data = $this->db->query("
    		SELECT 
    			companies.id,
    			companies.group_name,
    			companies.customer_group_name,
    			companies.name,
    			companies.company,
    			companies.vat_no,
    			companies.address,
    			companies.city,
    			companies.state,
    			companies.postal_code,
    			companies.country,
    			companies.phone,
    			companies.email,
    			CASE WHEN sum(c_acc.amount)  < 0 then abs(sum(c_acc.amount)) ELSE 0 END as current_credit,
            	CASE WHEN sum(c_acc.amount)  > 0 then sum(c_acc.amount) ELSE 0 END as current_debit
    			FROM sma_companies as companies
    			LEFT JOIN sma_customer_accounts as c_acc
    			ON companies.`id` =  c_acc.`company_id`
    			WHERE c_acc.company_id = $company_id
    	")->row();
    	return $data;
    }

    public function addDebitBill($data = []) {
        $debit_data = [
            'created_at'        => date('Y-m-d H:i:s'),
            'bill_date'         => date('Y-m-d', strtotime($data['bill_date'])),
            'company_id'        => $data['company_id'],
            'amount'            => abs($data['amount']),
            'type'              => 'debit',
            'comment'           => $data['comment'],
            'collected_by'      => $this->session->userdata('user_id'),
            'created_by'        => $this->session->userdata('user_id'),
            'updated_by'        => NULL,
            'updated_at'        => NULL, 
            'is_deleted'        => 0,     
 
        ];
        
        $insert_debit = $this->db->insert('customer_accounts', $debit_data);
        return true;

    }

    public function formattBillData($data = []) {
        $credit_data = [
            'created_at'        => date('Y-m-d H:i:s'),
            'bill_date'         => date('Y-m-d', strtotime($data['bill_date'])),
            'company_id'        => $data['company_id'],
            'amount'            => (abs($data['amount']) * -1 ),
            'type'              => 'credit',
            'comment'           => $data['comment'],
            'collected_by'      => $this->session->userdata('user_id'),
            'created_by'        => $this->session->userdata('user_id'),
            'updated_by'        => NULL,
            'updated_at'        => NULL, 
            'is_deleted'        => 0,     
        ];

        $debit_data = [
            'created_at'        => date('Y-m-d H:i:s'),
            'bill_date'         => date('Y-m-d', strtotime($data['bill_date'])),
            'company_id'        => $data['company_id'],
            'amount'            => abs($data['paid_amount']),
            'type'              => 'debit',
            'comment'           => $data['comment'],
            'collected_by'      => $this->session->userdata('user_id'),
            'created_by'        => $this->session->userdata('user_id'),
            'updated_by'        => NULL,
            'updated_at'        => NULL, 
            'is_deleted'        => 0,     

        ];
        $insert_credit = $this->db->insert('customer_accounts', $credit_data);
        $insert_debit = $this->db->insert('customer_accounts', $debit_data);
        return true;
    }

    public function deleteItem($id) {
        if ($this->db->delete("customer_accounts", array('id' => $id))) {
            return true;
        }
        return FALSE;
    
    }
 }
